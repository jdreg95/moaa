from flask import Blueprint, request, Response,abort, jsonify
from app import app
from app.asset.model import Asset
from app.review.model import HumanReview, RobotReview
from app.helpers import validate_json, validate_schema, allowed_file, get_by_id
from werkzeug.utils import secure_filename
from flask_jwt_extended import jwt_required
from bson import json_util

import boto3, botocore

asset = Blueprint('asset', __name__)

s3 = boto3.client('s3', aws_access_key_id= app.config.get('S3_KEY'), aws_secret_access_key=app.config.get('S3_SECRET'))

# JSON schema's
human_review_schema = {
    "type" : ["object"],
    "properties" : {
        "author" : {"type" : "string"},
        "value" : {"type" : "integer"}
    }
}

robot_review_schema = {
    "type" : ["null", "object"],
    "properties" : {
        "value" : {"type" : "number"}
    }
}

# Keep schema fields nullable due to ease of developing prototype
asset_schema = {
    "type" : "object",
    "properties" : {
        "author" : {"type" : ["null", "string"]},
        "uri" : {"type" : ["null", "string"]},
        "location": {
            "type": ["null", "array"], 
            "items": [
                {
                    "type": "number"
                },
                {
                    "type": "number"
                }
            ]
        },
        "prediction": robot_review_schema,
        "reviews": { "type": ["null", "array"], "items": human_review_schema }
    }
}

# Get all assets
@asset.route('/assets', methods=['GET'])
@jwt_required
def get_assets():
    assets = Asset.objects.all()

    return Response(
        response=assets.to_json(),
        status=200,
        mimetype='application/json'
    )

# Get asset by id
@asset.route('/assets/<id>', methods=['GET'])
@jwt_required
@get_by_id(Asset)
def get_asset(asset, *args, **kwargs):
    return Response(
        response=asset.to_json(),
        status=200,
        mimetype='application/json'
    )

# Delete asset by id
@asset.route('/assets/<id>', methods=['DELETE'])
@jwt_required
@get_by_id(Asset)
def delete_asset(asset, *args, **kwargs):
    asset.delete()
    return Response(
        response=asset.to_json(),
        status=200,
        mimetype='application/json'
    )

# Add prediction to asset by id
@asset.route('/assets/<id>/prediction', methods=['PUT'])
@jwt_required
@validate_json
@validate_schema(robot_review_schema)
@get_by_id(Asset)
def update_prediction(asset, *args, **kwargs):
    assetJSON = request.json
    asset.prediction.value = assetJSON.get('value')
    asset.save()

    return Response(
        response=asset.to_json(),
        status=200,
        mimetype='application/json'
    )

# Add a review to asset by id
@asset.route('/assets/<id>/review', methods=['PUT'])
@jwt_required
@validate_json
@validate_schema(human_review_schema)
@get_by_id(Asset)
def add_review(asset, *args, **kwargs):
    assetJSON = request.json
    asset.reviews.create(author=assetJSON.get('author'), value=assetJSON.get('value'))
    asset.save()

    return Response(
        response=asset.to_json(),
        status=200,
        mimetype='application/json'
    )

# Create a new asset
@asset.route('/assets', methods=['POST'])
@jwt_required
@validate_json
@validate_schema(asset_schema)
def create_asset():
    assetJSON = request.json

    asset = Asset(
        author=assetJSON.get('author'),
        uri=assetJSON.get('uri'),
        location=assetJSON.get('location'),
        prediction=assetJSON.get('prediction'),
        reviews=assetJSON.get('reviews')
    )
    
    asset.save()

    return Response(
        response=asset.to_json(),
        status=200,
        mimetype='application/json'
    )

# Create a new image to an asset
@asset.route('/assets/picture', methods=['POST'])
@jwt_required
def create_asset_picture():
    # Handle file upload
    file = request.files['file']

    if not file or file.filename is '':
        print('NO FILE')
        return jsonify({'message': 'No file.', 'status': 400}), 400
    
    if not allowed_file(file.filename):
        print('FILE NOT ALLOWED')
        return jsonify({'message': 'File is not allowed.', 'status': 400}), 400
    
    # Upload to S3
    try:
        file.filename = secure_filename(file.filename)

        s3.upload_fileobj(
            file,
            app.config.get('S3_BUCKET'),
            '{}{}'.format(app.config.get('S3_IMAGE_FOLDER'), file.filename),
            ExtraArgs={
                'ContentType': file.content_type
            }
        )
        
        # Return S3 location for later use
        uri = '{}{}{}'.format(app.config.get('S3_LOC'), app.config.get('S3_IMAGE_FOLDER'), file.filename)
        return jsonify({ 'uri': uri }), 201
    except Exception as e:
        print('ERROR: ')
        print(e.message)

        return jsonify({'message': e.message, 'status': 400}), 400


    
    
